import sys
import math
n1, n2 , operator = sys.argv
def Calculate ( n1 , n2 , operator ) :
    ''' Basic Calculator functions'''
    if operator == '+' :
        result = n1 + n2
    elif operator == '-' :
        result = n1 - n2
    elif operator == '*' :
        result = n1*n2
    elif operator == '/' :
        try:
            result = n1/n2
        except ZeroDivisionError :
            print ( 'You cannot divide by zero ')
            return None
    elif operator == 'log':
        try:
            result = math.log(n1) / math.log(n2)
        except ValueError :
            return print ( ' Cannot take log of zero or negative numbers!')
        except TypeError :
            return print ( 'cannot take log of non-numeric value.')
        except Exception as ex:
            return print ( 'unhandled Exception!' , ex)
        else:
            return result

    else :
        print ( 'Unrecognized operator' , operator )
    return result
    