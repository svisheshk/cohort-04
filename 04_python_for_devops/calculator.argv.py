import math
import sys
from calculator import Calculate

# Function call
print(sys.argv)
result = Calculate(
    int(sys.argv[1]),
    int(sys.argv[2]),
    sys.argv[3])
#print('Executing script: ', sys.argv[0])
if result is None:
    print('Invalid calculation!')
else:
    print('The answer is', result)